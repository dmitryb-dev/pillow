#include "unity.h"
#include "util/utils.h"
#include "service/validation/value.h"
#include "service/validation/state.h"

StateValidator validator;

void setUp() {
	validator = state_validator_create_with_init_val(2, -1);
}
void tearDown() {}

void test_non_valid_when_not_enough_values()
{
	TEST_ASSERT_EQUAL(false, state_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(false, state_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(true, state_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(true, state_is_valid(&validator, 1));
}

void test_reset_when_value_deffers()
{
	TEST_ASSERT_EQUAL(false, state_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(false, state_is_valid(&validator, 2));
	
	TEST_ASSERT_EQUAL(false, state_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(false, state_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(true, state_is_valid(&validator, 1));
}