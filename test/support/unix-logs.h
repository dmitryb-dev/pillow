#define ENABLE_LOGGING

#include <stdio.h>
#include "platform/log.h"
#include "util/log.h"

void io_log(const char* msg)
{
	printf("%s", msg);
}