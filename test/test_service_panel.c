#include "unity.h"

#include "support/unix-logs.h"
#include "util/utils.h"
#include "service/panel.h"
#include "service/validation/state.h"
#include "service/validation/value.h"

#include "mock_io.h"
#include "mock_input-validator.h"

PanelService panel_service;

ValidatorService mode_service;
ValidatorService volume_service;
ValidatorService duration_service;

void setUp() 
{	
	validator_create_with_init_val_IgnoreAndReturn(mode_service);
	validator_create_IgnoreAndReturn(volume_service);
	validator_create_IgnoreAndReturn(duration_service);
	
	panel_service = panel_create();
}
void tearDown() {}

void test_dlegates_to_sub_services() 
{
	input_get_validated_ExpectAndReturn(&mode_service, 1);
	TEST_ASSERT_EQUAL(1, panel_get_mode(&panel_service));
	
	input_get_validated_ExpectAndReturn(&volume_service, 2);
	TEST_ASSERT_EQUAL(2, panel_get_volume(&panel_service));
	
	input_get_validated_ExpectAndReturn(&duration_service, 3);
	TEST_ASSERT_EQUAL(3, panel_get_duration(&panel_service));
}

