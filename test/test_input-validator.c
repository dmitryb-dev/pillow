#include "unity.h"
#include "support/unix-logs.h"
#include "service/validation/input-validator.h"
#include "mock_io.h"
#include "mock_functions-mocks.h"

ValidatorService service;
void* validator = 0;

void setUp() 
{	
	service = validator_create(WORKING_MODE_TOGGLE, validator, is_valid_f_mock);
}
void tearDown() {}

void test_return_last_valid_mode() 
{
	io_get_ExpectAndReturn(WORKING_MODE_TOGGLE, 7);
	is_valid_f_mock_ExpectAndReturn(validator, 7, true);
	TEST_ASSERT_EQUAL(7, input_get_validated(&service));
	
	io_get_ExpectAndReturn(WORKING_MODE_TOGGLE, 10);
	is_valid_f_mock_ExpectAndReturn(validator, 10, true);
	TEST_ASSERT_EQUAL(10, input_get_validated(&service));
	
	io_get_ExpectAndReturn(WORKING_MODE_TOGGLE, 3);
	is_valid_f_mock_ExpectAndReturn(validator, 3, false);
	TEST_ASSERT_EQUAL(10, input_get_validated(&service));
	
	io_get_ExpectAndReturn(WORKING_MODE_TOGGLE, 5);
	is_valid_f_mock_ExpectAndReturn(validator, 5, false);
	TEST_ASSERT_EQUAL(10, input_get_validated(&service));
	
	io_get_ExpectAndReturn(WORKING_MODE_TOGGLE, 12);
	is_valid_f_mock_ExpectAndReturn(validator, 12, true);
	TEST_ASSERT_EQUAL(12, input_get_validated(&service));
}

