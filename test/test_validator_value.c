#include "unity.h"
#include "util/utils.h"
#include "service/validation/value.h"

ValueValidator validator;

void setUp() {
	validator = value_validator_create_with_init_val(2, 2, -10);
}
void tearDown() {}

void test_non_valid_when_not_enough_values()
{
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 2));
	TEST_ASSERT_EQUAL(true, value_is_valid(&validator, 3));
}

void test_reset_when_max_deviation_exceeded()
{
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 4)); // too big, 4 becomes origin
	
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 2));
	TEST_ASSERT_EQUAL(true, value_is_valid(&validator, 3));
}

void test_correct_working_with_negative_values()
{
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 2));
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, -1));
	
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(true, value_is_valid(&validator, -1));
}

void test_filter_races()
{
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 1));
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 2));
	TEST_ASSERT_EQUAL(true, value_is_valid(&validator, 1));
	
	TEST_ASSERT_EQUAL(true, value_is_valid(&validator, 3));
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 12)); // too big deviation
	TEST_ASSERT_EQUAL(false, value_is_valid(&validator, 11));
	TEST_ASSERT_EQUAL(true, value_is_valid(&validator, 13)); // seems value is ok
}