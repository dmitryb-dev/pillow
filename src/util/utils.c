#include "utils.h"

char char_abs(char value)
{
	return value >= 0? value : -value;
}