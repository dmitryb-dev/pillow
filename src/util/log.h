#ifndef UTIL_LOG_H
#define UTIL_LOG_H

#include "../prefs.h"

#ifdef ENABLE_LOGGING
	#include <stdio.h>
	#include "../platform/log.h"

	#define LOG(msg) io_log(msg)

	char _conversion_buffer[10];
	#define LOG_NUMBER(value) \
		sprintf((char*) &_conversion_buffer, "%d", value); \
		LOG((char*) &_conversion_buffer)
#else
	#define LOG(msg)
	#define LOG_NUMBER(value)
#endif

#endif
