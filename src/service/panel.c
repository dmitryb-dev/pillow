#include "panel.h"
#include "../util/log.h"

PanelService panel_create()
{
	PanelService panel = {
		.mode_validator = state_validator_create_with_init_val(VALIDATION_THRESHOLD, OFF),
		
		.volume_validator = value_validator_create(VALIDATION_THRESHOLD, VALIDATION_MAX_DEVIATION),

		.duration_validator = value_validator_create(VALIDATION_THRESHOLD, VALIDATION_MAX_DEVIATION)
	};
	
	panel.mode_service = validator_create_with_init_val(WORKING_MODE_TOGGLE, &panel.mode_validator, (ValidatorF) state_is_valid, OFF);
	
	panel.volume_service = validator_create(VOLUME_SLIDER, &panel.volume_validator, (ValidatorF) value_is_valid);
	
	panel.duration_service = validator_create(DURATION_SLIDER, &panel.duration_validator, (ValidatorF) value_is_valid);
	
	return panel;
}

PillowMode panel_get_mode(PanelService* service)
{
	LOG("Panel: get mode\n");
	return input_get_validated(&service->mode_service);
}

char panel_get_volume(PanelService* service)
{
	LOG("Panel: get volume\n");
	return input_get_validated(&service->volume_service);
}

char panel_get_duration(PanelService* service)
{
	LOG("Panel: get duration\n");
	return input_get_validated(&service->duration_service);
}