#ifndef SERVICE_AUDIO_H
#define SERVICE_AUDIO_H

#include "../util/utils.h"
#include "validation/value.h"

typedef struct {
	ValueValidator value_validator;
	ValidatorService validation_service;
	
	// 0 - far, 10 - near 
	char max_distance_to_turn_on;
} HeadSensorService;

HeadSensorService head_sensor_create()
{
	HeadSensorService service = {
		.value_validator = value_validator_create(HEAD_SENSOR_VALIDATION_THRESHOLD, HEAD_SENSOR_MAX_DEVIATION),
		.max_distance_to_turn_on = 10 - HEAD_SENSOR_SENSITIVITY
	}
	service.validation_service = validator_create(HEAD_SENSOR, &service.value_validator, (ValidatorF) value_is_valid);
	return service;
}

bool head_sensor_is_head_on_pillow(HeadSensorService service)
{
	return input_get_validated(&service->validation_service);
}

#endif
