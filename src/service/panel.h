#ifndef SERVICE_PANEL_H
#define SERVICE_PANEL_H

#include "../prefs.h"
#include "validation/input-validator.h"
#include "validation/state.h"
#include "validation/value.h"

typedef struct {
	StateValidator mode_validator;
	ValueValidator volume_validator;
	ValueValidator duration_validator;
	
	ValidatorService mode_service;
	ValidatorService volume_service;
	ValidatorService duration_service;
} PanelService;

typedef enum {
	OFF,
	AUTO,
	MANUAL
} PillowMode;

PanelService panel_create();

PillowMode panel_get_mode(PanelService* service);

char panel_get_volume(PanelService* service);

char panel_get_duration(PanelService* service);

#endif
