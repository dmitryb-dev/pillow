#ifndef SERVICE_VALIDATING_H
#define SERVICE_VALIDATING_H

#include "../../util/utils.h"
#include "../../platform/io.h"

typedef bool (*ValidatorF)(void*, char);

typedef struct {
	InputPort port;
	void* validator_state;
	ValidatorF is_valid_f;
	char last_valid_value;
} ValidatorService;

/*
 * Params validator and is_valid_function - something from validation directory.
 */
ValidatorService validator_create_with_init_val(InputPort port, void* validator, ValidatorF is_valid_f, char init_val);
ValidatorService validator_create(InputPort port, void* validator, ValidatorF is_valid_f);

char input_get_validated(ValidatorService *service);

#endif
