#include "input-validator.h"
#include "../../util/log.h"

char input_get_validated(ValidatorService *service) 
{
	char new_value = io_get(service->port);
	if (service->is_valid_f(service->validator_state, new_value))
	{
		LOG("Valid value: "); LOG_NUMBER(new_value);
		service->last_valid_value = new_value; 
	}
	else
	{
		LOG("Not valid value: "); LOG_NUMBER(new_value); 
		LOG(", using prev: "); LOG_NUMBER(service->last_valid_value); 
	}
	LOG("\n");
	return service->last_valid_value;
}

ValidatorService validator_create_with_init_val(InputPort port, void* validator, ValidatorF is_valid_f, char init_val)
{
	ValidatorService service = {
		.port = port,
		.validator_state = validator,
		.is_valid_f = is_valid_f,
		.last_valid_value = init_val
	};
	return service;
}

ValidatorService validator_create(InputPort port, void* validator, ValidatorF is_valid_f)
{
	return validator_create_with_init_val(port, validator, is_valid_f, 0);
}