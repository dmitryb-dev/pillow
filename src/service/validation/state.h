#ifndef VALIDATOR_STATE_H
#define VALIDATOR_STATE_H

#include "value.h"

typedef struct {
	ValueValidator _decorated;
} StateValidator;

StateValidator state_validator_create_with_init_val(char threshold, char init_state);

StateValidator state_validator_create(char threshold);

bool state_is_valid(StateValidator *validator, char state);

#endif
