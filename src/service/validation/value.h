#ifndef VALIDATOR_VALUE_H
#define VALIDATOR_VALUE_H

#include "../../util/utils.h"

typedef struct {
	char max_deviation;
	char threshold;
  	char last_value;
	char _passed_values_counter;
} ValueValidator;

ValueValidator value_validator_create_with_init_val(char threshold, char max_deviation, char init_value);

ValueValidator value_validator_create(char threshold, char max_deviation);

bool value_is_valid(ValueValidator *validator, char value);

#endif
