#include "value.h"

bool value_is_valid(ValueValidator *validator, char value)
{
	if (char_abs(validator->last_value - value) <= validator->max_deviation)
	{
		if (validator->_passed_values_counter < validator->threshold)
			validator->_passed_values_counter++; 
	}
	else
	{
		validator->last_value = value;
		validator->_passed_values_counter = 0;
	}
	return validator->_passed_values_counter >= validator->threshold;
}

ValueValidator value_validator_create_with_init_val(char threshold, char max_deviation, char init_value)
{
	ValueValidator validator = {
		.threshold = threshold,
		.max_deviation = max_deviation, 
		.last_value = init_value, 
		._passed_values_counter = 0
	};
	return validator;
}

ValueValidator value_validator_create(char threshold, char max_deviation)
{
	return value_validator_create_with_init_val(threshold, max_deviation, 0);
}