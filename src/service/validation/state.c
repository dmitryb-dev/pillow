#include "state.h"

bool state_is_valid(StateValidator *validator, char state)
{
	return value_is_valid(&validator->_decorated, state);
}

StateValidator state_validator_create_with_init_val(char threshold, char init_state)
{
	StateValidator validator = { 
		._decorated = value_validator_create_with_init_val(threshold, 0, init_state)
	};
	return validator;
}

StateValidator state_validator_create(char threshold)
{
	return state_validator_create_with_init_val(threshold, 0);
}