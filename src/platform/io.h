#ifndef PLATFORM_IO_H
#define PLATFORM_IO_H

typedef enum {
	WORKING_MODE_TOGGLE, 	// 0, 1, 2
	VOLUME_SLIDER, 			// 0-100
	DURATION_SLIDER, 		// 0-100
	HEAD_SENSOR, 			// 0 - head out, 1-10 head in (1 - far, 10 - near)
	BATTERY_LEVEL 			// 0-10
} InputPort;

typedef enum {
	SOUND, 			// (-100)-100
	SLEEP_COMMAND 	// 0-1
} OutputPort;

void io_push(OutputPort port, char value);
char io_get(InputPort port);

#endif
