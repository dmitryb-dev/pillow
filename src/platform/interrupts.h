#ifndef PLATFORM_INTERRUPTS_H
#define PLATFORM_INTERRUPTS_H

void interrupts_register_timer(int interval_millis, void (*callback)(void));
void interrupts_register_wake_up(void (*callback)(void));

#endif
