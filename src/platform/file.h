#ifndef PLATFORM_FILE_H
#define PLATFORM_FILE_H

int file_size(char file_id);
char file_read(char file_id, int index);

#endif
