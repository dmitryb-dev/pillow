#ifndef PREFERENCES_H
#define PREFERENCES_H

#define ENABLE_LOGGING

// How many times values checks until it will be validated.
#define VALIDATION_THRESHOLD 2

// Max value difference to pass validation.
#define VALIDATION_MAX_DEVIATION 3

// 0 - 10 inclusive
#define HEAD_SENSOR_SENSITIVITY 3

#define HEAD_SENSOR_VALIDATION_THRESHOLD 7
#define HEAD_SENSOR_MAX_DEVIATION 0

#endif
